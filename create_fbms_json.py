"""Create COCO like annotations of FBMS.

See the COCO format here: <http://cocodataset.org/#format-data>

Some differences from COCO:

(1) 'image' structure:
    (a) "license": Set to "unknown".
    (b) "flickr_url", "coco_url", "date_captured": These fields are removed.
"""

import argparse
import collections
import json
import logging
import itertools
import utils
from datetime import datetime
from pathlib import Path
from pprint import pformat

import numpy as np
import pycocotools.mask as mask_util
from PIL import Image
from skimage import measure
from tqdm import tqdm

EXPECTED_NUM_SEQUENCES = {
    'TrainingSet': 29,
    'TestSet': 30
}


def binary_mask_to_polygon(binary_mask):
    """Converts a binary mask to COCO polygon representation.

    Adapted from:
    <https://github.com/waspinator/pycococreator/blob/db5efbf7be77024c81af2ad41eb5338b81c0e4bc/pycococreatortools/pycococreatortools.py>

    Args:
        binary_mask: a 2D binary numpy array where '1's represent the object
    """
    polygons = []
    # Pad mask to close contours of shapes which start and end at an edge
    padded_binary_mask = np.pad(
        binary_mask, pad_width=1, mode='constant', constant_values=0)
    contours = measure.find_contours(padded_binary_mask, 0.5)
    contours = np.subtract(contours, 1)
    for contour in contours:  # contour is (n, 2) array of points
        if contour.shape[0] < 3:
            continue  # Polygons must have at least 3 points.
        if not np.array_equal(contour[0], contour[-1]):
            contour = np.vstack((contour, contour[0]))  # Close the contour
        contour = np.flip(contour, axis=1)  # (y, x) -> (x, y) points
        # After padding and subtracting 1 we may get -0.5 points in our
        # segmentation.
        contour = contour.clip(min=0)
        segmentation = contour.ravel().tolist()
        polygons.append(segmentation)
    return polygons


def main():
    # Use first line of file docstring as description if it exists.
    parser = argparse.ArgumentParser(
        description=__doc__.split('\n')[0] if __doc__ else '',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--fbms-root', required=True)
    parser.add_argument('--output-dir', required=True)

    args = parser.parse_args()

    fbms_root = Path(args.fbms_root)
    if not fbms_root.exists():
        raise ValueError('--fbms-root %s does not exist' % args.fbms_root)
    for split in ['TrainingSet', 'TestSet']:
        expected_path = fbms_root / split
        if not expected_path.exists():
            raise ValueError(
                'Split %s does not exist at %s' % (split, expected_path))

    output_root = Path(args.output_dir)
    output_root.mkdir(exist_ok=True, parents=True)
    logging_path = str(output_root / (Path(__file__).stem + '.log'))
    utils.setup_logging(logging_path)
    file_logger = logging.getLogger(logging_path)
    logging.info('Args: %s' % pformat(vars(args)))

    file_logger.info('Source:')
    file_logger.info('======')
    with open(__file__, 'r') as f:
        file_logger.info(f.read())
    file_logger.info('======')

    date = datetime.now()
    dataset_info = {
        'year': date.year,
        'version': '1.0',
        'description': 'FBMS dataset in COCO format.',
        'contributor': ('Dataset from Peter Ochs, Jitendra Malik, Thomas Brox.'
                        ' COCO format by Achal Dave.'),
        'url': ('https://lmb.informatik.uni-freiburg.de/resources/datasets/'
                'moseg.en.html'),
        'date_created': str(date)
    }
    long_license = 'Unknown; FBMS does not provide a license.'
    categories = [{'supercategory': 'object', 'id': 1, 'name': 'object'}]
    output_info = {
        'TrainingSet': {
            'info': dataset_info,
            'images': [],
            'annotations': [],
            'categories': categories,
            'licenses': [long_license]
        },
        'TestSet': {
            'info': dataset_info,
            'images': [],
            'annotations': [],
            'categories': categories,
            'licenses': [long_license]
        }
    }

    sequences = []
    for split in ['TrainingSet', 'TestSet']:
        split_path = fbms_root / split
        split_sequences = [x for x in split_path.iterdir() if x.is_dir()]
        if len(split_sequences) != EXPECTED_NUM_SEQUENCES[split]:
            raise ValueError('Expected %s sequences in %s, found %s' %
                             (EXPECTED_NUM_SEQUENCES[split], split_path,
                              len(split_sequences)))
        logging.info(
            'Loaded %s sequences for split %s' % (len(split_sequences), split))
        sequences.extend(split_sequences)

    image_id_generator = itertools.count()
    annotation_id_generator = itertools.count()
    for sequence in tqdm(sequences):
        split = sequence.relative_to(fbms_root).parts[0]
        groundtruth = utils.FbmsGroundtruth(sequence / 'GroundTruth')
        frame_paths = sorted(sequence.glob('*.jpg'), key=utils.get_framenumber)
        for frame_number, frame_labels in groundtruth.frame_labels().items():
            frame_path = frame_paths[frame_number]
            width, height = Image.open(frame_path).size
            image_info = {
                'id': next(image_id_generator),
                'width': width,
                'height': height,
                'file_name': str(frame_path.relative_to(fbms_root)),
                'license': 0,
            }
            output_info[split]['images'].append(image_info)

            masks = []
            for color, region_id in groundtruth.color_to_region.items():
                if region_id == 0:
                    # ppms have full white (255 * 256**2 + 255 * 256 + 255)
                    # as background, pgms have 0 as background.
                    assert color == 16777215 or color == 0
                    continue  # Ignore background
                mask = (frame_labels == region_id)
                if mask.any():
                    masks.append(frame_labels == region_id)
            if not masks:
                raise ValueError('No masks found for frame %s' % frame_path)

            polygons = [binary_mask_to_polygon(mask) for mask in masks]

            masks_np = np.array(masks, dtype=np.uint8).transpose(1, 2, 0)
            rle_masks = mask_util.encode(np.asfortranarray(masks_np))
            assert len(rle_masks) == len(masks)
            assert rle_masks[0]['size'] == list(masks[0].shape)
            areas = mask_util.area(rle_masks)
            boxes = mask_util.toBbox(rle_masks)
            for polygon, area, box in zip(polygons, areas, boxes):
                if not polygon:
                    logging.warn(
                        'Empty polygon for annotation in %s' % frame_path)
                output_info[split]['annotations'].append({
                    'id': next(annotation_id_generator),
                    'image_id': image_info['id'],
                    'category_id': 1,
                    'segmentation': polygon,
                    'area': area.item(),
                    'bbox': box.tolist(),
                    'iscrowd': 0
                })

    for split, split_data in output_info.items():
        image_id_counts = collections.Counter(
            [x['id'] for x in split_data['images']])
        annotation_id_counts = collections.Counter(
            [x['id'] for x in split_data['annotations']])

        image_id_duplicates = {
            image_id: count
            for image_id, count in image_id_counts.items() if count > 2
        }
        annotation_id_duplicates = {
            annotation_id: count
            for annotation_id, count in annotation_id_counts.items()
            if count > 2
        }

        assert not image_id_duplicates, (
            'Image ids duplicated in split %s. Duplicate counts: %s' %
            (split, image_id_duplicates))
        assert not annotation_id_duplicates, (
            'Annotation ids duplicated in split %s. Duplicate counts: %s' %
            (split, annotation_id_duplicates))

    num_images = next(image_id_generator)
    num_annotations = next(annotation_id_generator)
    logging.info('Num images: %s, Num annotations: %s, '
                 'Avg annotations / image: %.02f' %
                 (num_images, num_annotations, num_annotations / num_images))

    train_json = output_root / 'train.json'
    test_json = output_root / 'test.json'
    with open(train_json, 'w') as f:
        json.dump(output_info['TrainingSet'], f)
    with open(test_json, 'w') as f:
        json.dump(output_info['TestSet'], f)


if __name__ == "__main__":
    main()

import itertools
import collections
import logging
import re
from pathlib import Path

import numpy as np
from PIL import Image
from tqdm import tqdm


def get_frameoffset(sequence, frame_index):
    """Return frame offset starting at 0.

    The tennis and marple4 sequences don't start at 1, which makes life
    difficult. This function normalizes the frame indices by returning the
    frame offset starting at 0 for all sequences."""
    if sequence == 'tennis':
        return frame_index - 454
    elif sequence == 'marple4':
        return frame_index - 324
    else:
        return frame_index - 1


def get_framenumber(filename):
    if not isinstance(filename, Path):
        filename = Path(filename)
    stem = filename.stem
    if stem.startswith('tennis'):
        return int(stem.split('tennis')[1])
    else:
        return int(stem.split('_')[1])


def setup_logging(logging_filepath):
    """Setup root logger to log to file and stdout.

    All calls to logging will log to `logging_filepath` as well as stdout.
    Also creates a file logger that only logs to , which can
    be retrieved with logging.getLogger(logging_filepath).

    Args:
        logging_filepath (str): Path to log to.
    """
    log_format = ('%(asctime)s %(filename)s:%(lineno)4d: ' '%(message)s')
    stream_date_format = '%H:%M:%S'
    file_date_format = '%m/%d %H:%M:%S'

    # Clear any previous changes to logging.
    logging.root.handlers = []
    logging.root.setLevel(logging.INFO)

    file_handler = logging.FileHandler(logging_filepath)
    file_handler.setFormatter(
        logging.Formatter(log_format, datefmt=file_date_format))
    logging.root.addHandler(file_handler)

    # Logger that logs only to file. We could also do this with levels, but
    # this allows logging specific messages regardless of level to the file
    # only (e.g. to save the diff of the current file to the log file).
    file_logger = logging.getLogger(logging_filepath)
    file_logger.addHandler(file_handler)
    file_logger.propagate = False

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(
        logging.Formatter(log_format, datefmt=stream_date_format))
    logging.root.addHandler(console_handler)

    logging.info('Writing log file to %s', logging_filepath)


class FbmsGroundtruth:
    def __init__(self, groundtruth_dir, include_unknown_labels=False):
        """
        Args:
            groundtruth_dir (pathlib.Path): Should contain a single .dat file
                of FBMS groundtruth. E.g.
                    {FBMS_ROOT}/TestSet/tennis/GroundTruth/

        Attributes:
            groundtruth_path (Path): Path to groundtruth directory.
            num_frames (int): Total number of frames in the video
            num_labeled_frames (int): Number of labeled frames in the video.
            image_width (int)
            image_height (int)
            color_to_region (dict): Map color in FBMS groundtruth files to
                region id.
            frame_label_paths (dict): Map FBMS frame number to groundtruth
                label path. Note: FBMS frame numbers in the evaluation start
                from 0, even though the groundtruth paths / image paths may
                start from 1, or some arbitrary number.
        """
        self.groundtruth_path = groundtruth_dir
        groundtruth_files = list(groundtruth_dir.glob('*.dat'))
        if len(groundtruth_files) == 0:
            raise ValueError('Found no .dat files in %s' % groundtruth_dir)
        elif len(groundtruth_files) > 1:
            raise ValueError(
                'Found multiple .dat files in %s' % groundtruth_dir)
        groundtruth_file = groundtruth_files[0]

        next_check = FbmsGroundtruth.next_check
        self.image_width, self.image_height = None, None
        with open(groundtruth_dir / groundtruth_file, 'r') as f:
            lines = (x.strip() for x in f.readlines())
            # Ignore first 3 lines
            next_check(lines, 'Ground truth definition file; do not change!')
            next_check(lines, '')
            next_check(lines, 'Total number of regions:')

            self.num_regions = int(next(lines))
            self.color_to_region = {}
            for r in range(self.num_regions):
                next_check(lines, '')
                self.color_to_region[int(next(lines))] = r

            next_check(lines, '')
            next_check(lines, 'Confusion penality matrix')
            # Ignore confusion penalty matrix
            for _ in range(self.num_regions):
                next(lines)
            next_check(lines, '')

            next_check(lines, 'Total number of frames in this shot:')
            self.num_frames = int(next_check(lines, '[0-9]*'))

            next_check(lines, 'Total number of labeled frames for this shot:')
            self.num_labeled_frames = int(next_check(lines, '[0-9]*'))

            # Map frame number to file path
            self.frame_label_paths = {}
            for _ in range(self.num_labeled_frames):
                next_check(lines, 'Frame number:')
                frame_number = int(next_check(lines, '[0-9]*'))

                next_check(lines, 'File name:')
                file_name = next(lines)

                next_check(lines, 'Input file name:')
                next(lines)

                frame_path = groundtruth_dir / file_name
                self.frame_label_paths[frame_number] = frame_path
                if self.image_height is None:
                    self.image_width, self.image_height = (
                        Image.open(frame_path).size)

    def _load_label_path(self, label_path):
        frame_labels = np.asarray(
            Image.open(label_path.resolve()))
        if label_path.suffix == '.ppm':
            # region index is red*256^2 + green*256^1 + blue*256^0
            frame_labels = (
                frame_labels[:, :, 0] * 65536 +
                frame_labels[:, :, 1] * 256 + frame_labels[:, :, 2])
        return frame_labels

    def frame_labels(self, include_unknown_labels=False):
        """
        Args:
            include_unknown_labels (bool): Whether to include labels that
                are unexpected based on the groundtruth <sequence>Def.dat file.
                The <sequence>Def.dat file lists the regions in each sequence
                of FBMS; however, some sequences have frames with labeled
                regions that are not in this list. By default, the official
                FBMS evaluation code treats these (implicitly) as background
                regions*, which is what this code also does by default.

                * The official code maintains a global color_to_region array of
                length num_possible_colors to a region id, and only fills it in
                for the regions in the dat file. Since the array is global, it
                is initialized to 0, so unknown regions get assigned an id of
                0, which is the background id.

        Returns:
            labeled_frames (dict): Maps frame number to numpy array of size
                (height, width), containing the region each pixel belongs to.
        """
        labeled_frames = {}
        if include_unknown_labels:
            color_to_region = self.color_to_region.copy()
            region_id_generator = itertools.count(self.num_regions)
        else:
            color_to_region = self.color_to_region

        for frame_number, frame_label_path in sorted(
                self.frame_label_paths.items(), key=lambda x: x[0]):
            # TODO(achald): Use probability map files for pgm labels.
            # The official evaluation code uses, for every ppm file, a
            # corresponding pgm file that indicates the probability of each
            # pixel belonging to a region. Right now, we ignore that.
            frame_labels = self._load_label_path(frame_label_path.resolve())
            frame_labels_set = set(np.unique(frame_labels))
            region_labels_set = set(color_to_region.keys())

            unknown_labels = frame_labels_set.difference(region_labels_set)
            if unknown_labels and include_unknown_labels:
                logging.debug('Color-region map before update: %s',
                              color_to_region)
                color_to_region.update({
                    color: next(region_id_generator)
                    for color in sorted(unknown_labels)
                })
                logging.debug('Color-region map after update: %s',
                              color_to_region)
            elif unknown_labels:
                logging.warn(
                    'Unknown labels in frame (%s) not found in definion file '
                    'labels (%s), for file %s' %
                    (unknown_labels, region_labels_set, frame_label_path))

            new_frame_labels = np.zeros_like(frame_labels)
            color_values = np.unique(frame_labels)
            for color in color_values:
                if color in color_to_region:
                    region = color_to_region[color]
                else:
                    assert not include_unknown_labels, (
                        "include_unknown_labels true, but can't find label "
                        "in color_to_region map.")
                    region = 0
                new_frame_labels[frame_labels == color] = region
            labeled_frames[frame_number] = new_frame_labels
        return labeled_frames

    @staticmethod
    def next_check(generator, regex):
        x = next(generator)
        if not re.match(regex, x):
            raise ValueError('Expected regex %s, saw line %s' % (regex, x))
        return x


def masks_to_tracks(frame_segmentations):
    """
    Args:
        frame_segmentations (dict): Map frame number to numpy array containing
            segmentation of the frame. Each segmentation is a (height, width)
            numpy array where each pixel indicates the label/index of the
            object at that pixel.

    Returns:
        tracks (list): List of num_tracks elements. Each element contains a
            tuple of (points, label), where points is a list of (x, y,
            frame_number) tuples, and label is an integer. In this
            implementation, each track is a single pixel in a single frame.
    """
    tracks = []
    for frame_number, frame_labels in frame_segmentations.items():
        for y, x in itertools.product(
                range(frame_labels.shape[0]), range(frame_labels.shape[1])):
            label = frame_labels[y, x]
            tracks.append((x, y, frame_number, label))
    return [([(x, y, t)], label) for x, y, t, label in tracks]


def parse_tracks(track_str,
                 image_shape,
                 track_label_size_space_separated=False,
                 progress=True):
    """
    Parse text representation of tracks as in FBMS .dat files.

    The format is
            <num_frames>
            <num_tracks>
            <track1>
            <track2>
            ...
            <trackn>

        where each <track> is of the format
            <track_label>
            <track_size>
            <x> <y> <frame>
            ...
            <x> <y> <frame>

    Args:
        track_str (str)
        image_shape (tuple): (height, width) of image.
        track_label_size_space_separated (bool): The FBMS code requires that
            there is a newline between <track label> and <track size>, but
            one of the outputs I downloaded from a method has the two fields
            separated by a space. This boolean allows parsing such files.

    Returns:
        segmentation (np.array): Shape (num_frames, height, width), indicating
            the track that each pixel belongs to, or -1.
    """
    track_lines = track_str.strip().split('\n')
    num_frames = int(track_lines[0])
    num_tracks = int(track_lines[1])

    line_iterator = enumerate(track_lines[2:])

    height, width = image_shape
    segmentation = np.zeros((num_frames, height, width)) - 1

    # TODO(achald): Share this with FbmsGroundtruth.next_check
    def next_parse(parser, line_iterator):
        line_no, line = next(line_iterator)
        try:
            return parser(line)
        except ValueError:
            logging.error('Parsing error at line %s: "%s"' % (line_no, line))
            raise

    def parse_single_track(line_iterator, segmentation,
                           track_label_size_space_separated):
        if track_label_size_space_separated:
            def parse_label_size(x):
                x = x.split(' ')
                if len(x) != 2:
                    raise ValueError('Unable to parse track label size')
                return int(x[0]), int(x[1])
            label, size = next_parse(parse_label_size, line_iterator)
        else:
            label = next_parse(int, line_iterator)
            size = next_parse(int, line_iterator)
        for i in range(size):
            x, y, frame = next_parse(
                lambda x: [int(i) for i in x.split(' ')], line_iterator)
            segmentation[frame, y, x] = label

    for i in tqdm(range(num_tracks), disable=not progress):
        parse_single_track(line_iterator, segmentation,
                           track_label_size_space_separated)

    # Ensure that we've exhausted all the lines.
    try:
        leftover_line_no, _ = next(line_iterator)
    except StopIteration:
        pass  # Good! We've used up all the frames.
    else:
        raise ValueError(
            'Lines leftover after parsing (from line %s):\n%s' %
            (leftover_line_no, '\n'.join(track_lines[2 + leftover_line_no])))

    assert np.all(segmentation != -1)
    return segmentation


def load_ppm_segmentation(ppm_file):
    # Shape (height, width, 3)
    raw_segmentation = np.array(Image.open(ppm_file))
    return (raw_segmentation[:, :, 2] * 256**2 +
            raw_segmentation[:, :, 1] * 256 +
            raw_segmentation[:, :, 0])


def get_tracks_text(tracks, num_frames, verbose=False):
    """
    Args:
        tracks (list): List of num_tracks elements. Each element contains a
            tuple of (points, label), where points is a list of (x, y,
            frame_number) tuples, and label is an integer.
        num_frames (int)

    Returns:
        Text representation of tracks as required by FBMS. See parse_tracks for
        details.
    """
    output = "{num_frames}\n{num_tracks}\n{tracks}"

    track_format = "{track_label}\n{track_size}\n{points}"

    point_format = "{x} {y} {frame}\n"

    tracks_str = ''
    num_tracks = len(tracks)
    for track_points, track_label in tqdm(
            tracks, disable=not verbose, desc='tracks-to-text'):
        points = ''.join([
            point_format.format(x=x, y=y, frame=frame_number)
            for x, y, frame_number in track_points
        ])
        tracks_str += track_format.format(
            track_label=int(track_label),
            track_size=len(track_points),
            points=points)

    return output.format(
        num_frames=num_frames,
        num_tracks=num_tracks,
        tracks=tracks_str)
